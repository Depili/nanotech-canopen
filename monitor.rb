# Sits between the serial device and a fifo to monitor and decode the bus
require 'serialport'
require 'colorize'
require_relative "can_frame.rb"


tty = ARGV.last
fifo = "can"

input = open(fifo, "r+") # the w+ means we don't block
sp = SerialPort.new(ARGV.last, 115200, 8, 1, SerialPort::NONE)

sp.write "S8\r"
sp.write "O\r"

open("/dev/tty", "r+") do |tty|
  Thread.new do
    loop do
      msg = +""
      loop do
        c = sp.getc
        msg << c
        break if c == "\r"
      end
      puts "<- #{msg.chomp}".blue
      begin
        puts CanFrame.decode(msg).to_s.blue
      rescue ArgumentError
      end
    end
  end

  loop do
    msg = input.readline
    puts "-> #{msg.chomp}".yellow
    begin
      puts CanFrame.decode(msg).to_s.yellow
    rescue ArgumentError
    end
    sp.write "#{msg}".sub("\n", "\r")
  end
end