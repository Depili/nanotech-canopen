
CobID = {
  0x000 => "Network management",
  0x080 => "Sync",
  0x180 => "TPDO1",
  0x200 => "RPDO1: Control word",
  0x300 => "RPDO2",
  0x400 => "RPDO3",
  0x700 => "Node status",
  0x600 => "SDO receive",
  0x580 => "SDO transmit"
}.freeze

SDO = {
  0x1000 => "Device type",
  0x1001 => "Error register",
  0x1003 => "Error log",
  0x1005 => "COB ID SYNC message",
  0x1009 => "Hardware version",
  0x100A => "Software version",
  0x100C => "Guard time",
  0x100D => "Life time factor",
  0x1010 => "Store parameters",
  0x1011 => "Restore parameters",
  0x1014 => "Emergency message",
  0x1017 => "Provider heartbeat time",
  0x1018 => "IDENTITY_OBJECT_VENDOR_ID",
  0x1400 => "RPDO1_COMMUNICATION_PARAMETER",
  0x1401 => "RPDO2_COMMUNICATION_PARAMETER",
  0x1402 => "RPDO3_COMMUNICATION_PARAMETER",
  0x1403 => "RPDO4_COMMUNICATION_PARAMETER",
  0x1600 => "RPDO1_MAPPING",
  0x1601 => "RPDO2_MAPPING",
  0x1602 => "RPDO3_MAPPING",
  0x1603 => "RPDO4_MAPPING",
  0x1800 => "TPDO1_COMMUNICATION_PARAMETER",
  0x1801 => "TPDO2_COMMUNICATION_PARAMETER",
  0x1802 => "TPDO3_COMMUNICATION_PARAMETER",
  0x1803 => "TPDO4_COMMUNICATION_PARAMETER",
  0x1A00 => "TPDO1_MAPPING",
  0x1A01 => "TPDO2_MAPPING",
  0x1A02 => "TPDO3_MAPPING",
  0x1A03 => "TPDO4_MAPPING",
  0x2000 => "Step mode",
  0x2001 => "Enable closed loop",
  0x2002 => "CL configuration",
  0x2003 => "AD converter",
  0x2004 => "Current control",
  0x2005 => "Can enable and baud rate",
  0x2006 => "Motor pole pairs",
  0x2007 => "Brake wait time",
  0x2008 => "Input debounce time",
  0x2009 => "Node id",
  0x200A => "CL is enabled",
  0x200B => "CL POSCNT offset",
  0x200C => "CL load angle curve",
  0x200D => "Encoder rotation direction change",
  0x200E => "DSPdrive current controller parameter",
  0x200F => "Speed mode controller type",
  0x2010 => "External reference run IO",
  0x2011 => "Encoder type",
  0x2012 => "CL Test current",
  0x2013 => "CL Test load angle",
  0x2015 => "Reset switch-on counter",
  0x2018 => "CL Test velocity",
  0x2019 => "Active emergency actions",
  0x2020 => "Emergency message lecagy format",
  0x2021 => "Motor id",
  0x2040 => "RX error counter",
  0x2041 => "Bit timing",
  0x603F => "Error code",
  0x6040 => "Control word",
  0x6041 => "Status word",
  0x6042 => "VL target velocity",
  0x6043 => "VL velocity demand",
  0x6044 => "VL velocity actual value",
  0x6046 => "VL velocity min/max amount",
  0x6048 => "VL velocity acceleration",
  0x6049 => "VL velocity deceleration",
  0x604A => "VL velocity quick stop",
  0x604C => "VL dimension factor",
  0x605A => "Quick stop option code",
  0x6060 => "Modes of operation",
  0x6061 => "Modes of operation display",
  0x6062 => "Position demand value",
  0x6063 => "Internal Encoder position value",
  0x6064 => "Actual position",
  0x6065 => "Following error window",
  0x6066 => "Following error timeout",
  0x6067 => "Position window",
  0x6068 => "Position window time",
  0x6071 => "Target torque",
  0x607A => "End postion",
  0x607B => "POSITION_RANGE_LIMIT",
  0x607C => "Home offset",
  0x607D => "SOFTWARE_POSITION_LIMIT",
  0x607E => "Polarity (rotation direction)",
  0x607F => "Max profile velocity",
  0x6081 => "Profile velocity",
  0x6082 => "End velocity",
  0x6083 => "Profile acceleration",
  0x6084 => "Profile deceleration",
  0x6085 => "Quick stop deceleration",
  0x6086 => "Motion profile type (ramp type)",
  0x608D => "Acceleration notation index",
  0x608E => "Acceleration notation dimension",
  0x608F => "Position encoder resolution",
  0x6091 => "Gear ratio",
  0x6092 => "Feed constant",
  0x6098 => "Homing mode",
  0x6099 => "Home speeds",
  0x609A => "Homing acceleration",
  0x60A4 => "Profile jerk",
  0x60C0 => "Interpolation sub-mode select",
  0x60C1 => "Interpolation data record",
  0x60C2 => "Interpolation time period",
  0x60C4 => "Interpolation data configuration",
  0x60C5 => "Max acceleration",
  0x60C6 => "Max deceleration",
  0x60FD => "Digital inputs",
  0x60FE => "Digital outputs",
  0x6401 => "Analog input",
  0x6423 => "Analog input global interrupt enable",
  0x6424 => "Analog input interrupt upper limit",
  0x6425 => "Analog input interrupt lower limit",
  0x6426 => "Analog input interrupt delta",
  0x6427 => "Analog input interrupt negative delta",
  0x6428 => "Analog input interrupt positive delta",
  0x6502 => "Supported drive modes"
}.freeze

# Control word
# 0 - Switch on
# 1 - Enable voltage
# 2 - Quick stop, active low
# 3 - Enable operation
# 4-6 - Mode specific
# 7 - Fault reset
# 8 - Stop (mode specific)
# 9 - Mode specific
# 10 - Reserved
# 11-15 - Manufacturer specific
# PP MODE:
# 4 - Start travel order
# 5 - New order replaces current
# 6 - false: Absolute positioning, true: Relative postioning
# 8 - Halt
# 9 - Do not brake on chaning orders
# Homing mode
# 4 - Start referencing
# 8 - Halt
# IP mode
# 4 - Activate IP mode
# Torque mode
# 8 - true: stop, false: start

# Status word
# 0 - Ready to switch on
# 1 - Switched on
# 2 - Operation enabled
# 3 - Fault
# 4 - Voltage enabled
# 5 - Quick stop
# 6 - Switch on disabled
# 7 - Warning
# 8 - PLL sync complete
# 9 - Remote
# 10 - Target reached
# 11 - Internal limit active
# 12,13 - Mode specific
# 14,15 - Manufacturer specific
# PP Mode
# 12 - Set point ack
# 13 - Following error
# Homing mode
# 12 - Homing attained
# 13 - Error
# IP mode
# 12 - IP mode active
# Torque mode
# Bit 10 - Bit 8
# 0 0 - Specified torque not attained
# 0 1 - Motor brakes
# 1 0 - Specified torque attained
# 1 1 - Motor idle