# Creates a lawicel serial format frame for s SDO
# express upload. type is a one of:
# C - uint8
# c - int8
# S - uint16
# s - int16
# L - uint32
# l - int32

def sdo_send(node, index,subindex, type, data)
  payload = []

  # Get the data size from the type indentifier
  case type
  when "C", "c"
    # one byte of data
    n = 3
  when "S", "s"
    # Two bytes of data
    n = 2
  when "L", "l"
    # Four bytes of data
    n = 1
  else
    raise ArgumentError
  end

  payload << sdo_byte1(1,n,true,true)
  payload << index
  payload << subindex
  payload << data
  s = payload.pack("CSC#{type}").bytes

  msg = +"t%02.2x8" % (0x600+node)
  msg << "%02.2X" % s[0]
  msg << "%02.2X" % s[1]
  msg << "%02.2X" % s[2]
  msg << "%02.2X" % s[3]
  (0..(3-n)).each do |i|
    msg << "%02.2X" % s[4+i]
  end
  return msg.ljust(21, "0")
end


def sdo_byte1(css,n,e,s)
  byte = 0
  byte += css << 5
  byte += n << 2
  byte += 0b10 if e
  byte += 0b1 if s
  return byte
end