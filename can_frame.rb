class CanFrame
  attr_accessor :extended, :rtr, :address, :data_length, :data
  attr_reader :flags

  SDO_ADDRESSES = [0x600, 0x580]

  def self.decode(frame)
    f = CanFrame.new
    return f.decode(frame)
  end

  # Decode a lawicel serial format frame
  def decode(frame)
    raise ArgumentError unless frame.is_a? String
    case frame[0]
    when "t"
      # Normal frame
      @extended = false
      @rtr = false
    when "r"
      # Normal rtr frame
      @extended = false
      @rtr = false
    when "T"
      # Extended normal frame
      @extended = true
      @rtr = false
    when "r"
      # Extended rtr frame
      @extended = true
      @rtr = true
    else
      raise ArgumentError
    end

    if extended
      @address = frame[1..8].to_i(16)
      @data_length = frame[9].to_i
    else
      @address = frame[1..3].to_i(16)
      @data_length = frame[4].to_i
    end

    puts address
    data_start = extended ? 10 : 5
    d = []
    (0...data_length).each do |i|
      start = data_start + (i*2)
      d << frame[start..start+1].to_i(16)
    end
    @data = d
    parse_sdo if sdo?
    self
  end

  def co_addr
    (@address & 127)
  end

  def cob_id
    (@address & 0b11110000000)
  end

  # Check if this might be a SDO frame
  def sdo?
    return false unless SDO_ADDRESSES.include? cob_id
    return false unless data_length == 8
    true
  end

  def parse_sdo
    raise ArgumentError unless data_length == 8
    flags = {}
    flags[:s] = (data[0] & 0b1)
    flags[:e] = (data[0] & 0b10) >> 1
    flags[:n] = (data[0] & 0b1100) >> 2
    flags[:ccs] = (data[0] & 0b1110_0000) >> 5
    flags[:index] = data[1] + (data[2] << 8)
    flags[:subindex] = data[3]
    @flags = flags
  end

  def to_s
    str = +""
    str << (extended ? "E" : " ")
    str << (rtr ? "R" : " ")
    str << ("%2.2x" % address)
    str << (" %1.1x " % data_length)
    data.each do |d|
      str << "%02x " % d
    end
    str << " CO: Node: #{"%02.2X" % co_addr} COB ID: #{"%03.3X" % cob_id}"
    if sdo?
      str << " SDO css: #{flags[:ccs]} n: #{flags[:n]} e: #{flags[:e]} s: #{flags[:s]}"
      str << " Index: #{"%04.4X" % flags[:index]} Sub: #{"%2.2X" % flags[:subindex]}"
      str << " Payload: "
      (0..(3-flags[:n])).each do |i|
        str << "%02.2X" % data[4+i]
      end
    end
    return str
  end
end